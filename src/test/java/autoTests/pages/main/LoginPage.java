package autoTests.pages.main;

import autoTests.ConfigurationVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by R2D2 on 24.02.2016.
 */
public class LoginPage {
    WebDriver driver;
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    // Variables

    @FindBy(id = "UserName")
    public WebElement login;

    @FindBy (id = "UserPass")
    public WebElement pass;

    @FindBy (className = "bt_login")
    public WebElement buttonLogin;

    // Methods

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void getPage (){
        driver.get(configVariables.baseUrl);
    }
}
