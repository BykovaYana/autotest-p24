package autoTests.pages.main;

import autoTests.ConfigurationVariables;
import autoTests.PaymentModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import javax.xml.ws.WebFault;

/**
 * Created by R2D2 on 29.02.2016.
 */
public class ObjectPaymentPage
{
    WebDriver driver;
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    // Variables


    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[1]/ul/li[1]")
    public WebElement copyPayment;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[1]/ul/li[2]")
    public WebElement changePayment;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[1]/ul/li[3]")
    public WebElement removalPayment;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[1]/tbody/tr[1]/td[2]/span")
    public WebElement paymentNumber;

    //счет получателя
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[1]/td[2]/span")
    public WebElement recipientAccount;

    //счет отправителя
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[1]/tbody/tr[2]/td[2]/span")
    public WebElement senderAccount;

    //счет ОКПО
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[2]/td[2]/span")
    public WebElement okpo;

    //наименование платежа
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[3]/td[2]/span")
    public WebElement paymentName;

    //MFO
    @FindBy(xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[4]/td[2]/span")
    public WebElement bankCode;

    //сума платежа
     @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[3]/tbody/tr/td[2]/span[1]")
     public WebElement paymentSum;

    //валюта платежа
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[3]/tbody/tr/td[2]/span[2]")
    public WebElement paymentCurrency;

    //детали платежа
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[4]/tbody/tr[1]/td[2]/span")
    public WebElement paymentDetails;

    //статус платежа
    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[4]/tbody/tr[2]/td[2]")
    public WebElement paymentStatus;

    //подписать платеж
    @FindBy (css = ".default_action_button.sign")
    public WebElement signPayment;

    @FindBy(css = ".default_action_button.send")
    public WebElement sendPaymentButton;


    // Methods

    public ObjectPaymentPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void CheckPaymentOptions (WebDriver driver, PaymentModel paymentModelObject){
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        Assert.assertEquals(objectPaymentPage.recipientAccount.getText(), paymentModelObject.getRecipientAccount());
        Assert.assertEquals(objectPaymentPage.okpo.getText(), paymentModelObject.getOkpo());
        String[] bufer = objectPaymentPage.bankCode.getText().split(" ");
        Assert.assertEquals(bufer[0], paymentModelObject.getBankCode());
        Assert.assertEquals(objectPaymentPage.paymentDetails.getText(), paymentModelObject.getPaymentDetails());
        Assert.assertEquals(objectPaymentPage.paymentName.getText(), paymentModelObject.getPaymentName());
        Assert.assertEquals(objectPaymentPage.paymentSum.getText(), paymentModelObject.getPaymentSum()+".00");
    }

}
