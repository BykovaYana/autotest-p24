package autoTests.pages.main;

import autoTests.ConfigurationVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by R2D2 on 24.02.2016.
 */
public class SettingPage
{
    WebDriver driver;
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    // Variables
    @FindBy(xpath = "html/body/div[1]/div[3]/div/div[3]/div[1]/div[3]/div/div[2]")
    public WebElement electronicSignature;


    // Methods

    public SettingPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void getPage (){
        driver.get(configVariables.settingurl);
    }
}

