package autoTests.pages.main;

import autoTests.ConfigurationVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by R2D2 on 24.02.2016.
 */
public class PaymentsPage
{
    WebDriver driver;
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    private WebDriverWait wait;

    // Variables
    @FindBy (className = "selectedOption")
    public WebElement firmSelection;

    @FindBy(xpath = "html/body/div[1]/div[1]/div/div[3]/div/div/div[2]/div[2]/ul/li[4]")
    public WebElement firmTestMSB;

    @FindBy (xpath = "html/body/div[1]/div[1]/div/div[3]/div/div/div[2]/div[2]/ul/li[3]")
    public WebElement firmProgrammersMSBTest;

    @FindBy (xpath = "html/body/div[1]/div[2]/div/ul/li[1]/a")
    public WebElement paymentsLink;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[1]/td[4]")
    public WebElement newObject;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[1]/td[7]/div/div/a")
    public WebElement paymentStatusButton;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[2]/div[1]/div/ul/li[1]")
    public WebElement filterPayments;

    @FindBy (css = ".toggleStatus.prepared")
    public WebElement onSigning;

    @FindBy (css = ".toggleAll")
    public WebElement allPayments;

    @FindBy (css = ".checkNone")
    public WebElement checkNone;

    @FindBy (css = ".toggleStatus.signed")
    public WebElement toBeSentToTheBank;

    @FindBy (css = ".toggleStatus.in_processing")
    public WebElement onThePerformance;

    @FindBy (css = ".toggleStatus.zabrakovan")
    public WebElement returned;

    @FindBy (css = ".toggleStatus.in_processingzabrakovansuccess")
    public WebElement sentToTheBank;

    @FindBy (css = ".toggleAll.checkbox")
    public WebElement chooseAll;

    @FindBy (css = ".group_delete")
    public WebElement deleteButton;

    @FindBy (xpath = "html/body/div[5]/div[2]/div/button[2]")
    public WebElement confirmDeletion;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[2]/div[1]/div/ul/li[3]")
    public WebElement massSigning;

    @FindBy (css = ".group_send")
    public WebElement sendButton;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/div[4]/div[2]/span[2]/a")
    public WebElement nextPage;


    // Methods

    public PaymentsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void NextPageButton (){
        if (nextPage.isDisplayed()){
            nextPage.click();
        }
    }

    public void GoToTheDesiredPage (int pageNumber) throws InterruptedException {
        int i=0;
        while (i != pageNumber){
            nextPage.click();
            i++;
        }
    }

    public WebElement FindPayment (WebDriver driver, int paymentNumber){
        WebElement payment = driver.findElement(By.xpath(
                "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr["+paymentNumber+"]/td[3]"));
        return payment;
    }

    public WebElement FindCheckbox (WebDriver driver, int i){
        WebElement checkbox = driver.findElement(By.xpath(
                "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + i + "]/td[1]/span"));
        return checkbox;
    }

    public WebElement FindStatusButton (WebDriver driver, int i){
        By statusBtnDynLocator = By.xpath(
                "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + i + "]/td[7]/div/div/a");
        return ( isElementPresent(statusBtnDynLocator)) ? driver.findElement(statusBtnDynLocator) : null;

    }

    public void getPage (){
        driver.get(configVariables.playmetsurl);
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
