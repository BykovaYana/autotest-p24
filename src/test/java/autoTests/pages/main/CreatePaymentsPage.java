package autoTests.pages.main;

import autoTests.ConfigurationVariables;
import autoTests.CustomMethods;
import autoTests.PaymentModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by R2D2 on 24.02.2016.
 */
public class CreatePaymentsPage extends CustomMethods
{
    WebDriver driver;
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    // Variables
    @FindBy (className = "createNewHighlight")
    public WebElement buttonCreatePayments;

    //счет получателя
    @FindBy(id = "B_ACC")
    public WebElement recipientAccount;

    //ОКПО
    @FindBy (id = "B_CRF")
    public WebElement okpo;

    //наименование платежа
    @FindBy (id = "B_NAME")
    public WebElement paymentName;

    //МФО
    @FindBy (id = "B_BIC")
    public WebElement bankCode;

    //детали платежа
    @FindBy (id = "DETAILS")
    public WebElement paymentDetails;

    //сумма платежа
    @FindBy (className = "WarMoney")
    public WebElement paymentSum;

    @FindBy (css = ".support_action_button.save")
    public WebElement paymentSave;

    @FindBy (css = ".support_action_button.save")
    public WebElement paymentSaveChange;

    @FindBy(xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[1]/td[2]/input")
    public WebElement recipientAccountChange;

    @FindBy(css = ".long.WarNum.limited.fixed")
    public WebElement bankCodeChange;

    @FindBy (xpath = "html/body/div[1]/div[3]/div/form/table[2]/tbody/tr[3]/td[2]/input" )
    public WebElement paymentNameChange;

    @FindBy (css = ".default_action_button.saveSign")
    public WebElement saveAndSingButton;

    @FindBy (css = ".support_action_button.clean")
    public WebElement cleanButton;

    @FindBy (css =".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons.ui-draggable.ui-resizable")
    public WebElement errorWindow;

    // Methods

    public CreatePaymentsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void CheckDoesTheEmptyField (WebDriver driver, int i) throws Exception {

        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        PaymentsPage paymentsPage = new PaymentsPage(driver);

        Thread.sleep(configVariables.pause);
        addStepToTheReport("3. Нажать кнопку очистить");
        createPaymentsPage.cleanButton.click();

        Assert.assertEquals(createPaymentsPage.recipientAccount.getText(),"");
        System.out.println(i + " Счет получчателя пусто");

        Assert.assertEquals(createPaymentsPage.bankCode.getText(),"");
        System.out.println(i + " МФО пусто");

        Assert.assertEquals(createPaymentsPage.paymentName.getText(),"");
        System.out.println(i + " Наименование платежа пусто");

        Assert.assertEquals(createPaymentsPage.okpo.getText(),"");
        System.out.println(i + " ОКПО пусто");

        Assert.assertEquals(createPaymentsPage.paymentSum.getText(),"");
        System.out.println(i + " Сумма пусто");

        Thread.sleep(configVariables.pause);

       // Assert.assertEquals(createPaymentsPage.paymentDetails.getText(),"");
        System.out.println(i + " Детали платежа пусто пусто");

        System.out.println(i + " Все поля платежа очищены");


        addStepToTheReport("4. Вернуться на тсраницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.longPause);
    }

    public void CompletePaymentOptions (WebDriver driver, PaymentModel paymentModelObject) throws Exception {

        addStepToTheReport("5. Заполнить реквизиты платежа");
        Thread.sleep(configVariables.longPause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(recipientAccount));
        recipientAccount.clear();
        recipientAccount.sendKeys(paymentModelObject.getRecipientAccount());

        okpo.clear();
        okpo.sendKeys(paymentModelObject.getOkpo());

        paymentName.clear();
        paymentName.sendKeys(paymentModelObject.getPaymentName());

        bankCode.clear();
        bankCode.sendKeys(paymentModelObject.getBankCode());
       // Thread.sleep(5000);

        paymentDetails.clear();
        paymentDetails.sendKeys(paymentModelObject.getPaymentDetails());

        paymentSum.clear();
        Thread.sleep(configVariables.longPause);
        paymentSum.clear();
        paymentSum.sendKeys(paymentModelObject.getPaymentSum());
    }


}
