package autoTests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestRunner extends SetupAndTeardown {

    TestSuite testSuite = new TestSuite();

    @Test(enabled = true, groups = {"Main"}, priority = 1)
    public void A1() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "авторизация",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A1_login(driver);
    }


    @Test(enabled = true, groups = {"Main"}, priority = 1)
    public void A2() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "сменить фирму(страница платежи)",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A2_Payments(driver);
    }

    @Test(enabled = true, groups = {"Main"}, priority = 1)
    public void A3() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "настройки",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A3_Setting(driver);
    }


   @Test(enabled = true, groups = {"Main", "Осн. функционал"}, priority = 1)
    public void A4() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "создание платежа",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A4_CreatePayment(driver);
    }

    @Test(enabled = true, groups = {"Main"}, priority = 1)
    public void A7() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "редактирование платежа",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A7_ChangePayment(driver);
    }


    @Test(enabled = true, groups = {"Main", "Осн. функционал"}, priority = 1)
    public void A8() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "копирование платежа",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A8_CopyPayment(driver);
    }

    @Test(enabled = true, groups = {"Main", "Осн. функционал"}, priority = 1)
    public void A5() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "удаление платежа",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A5_removalPayment(driver);
    }

    @Test(enabled = true, groups = {"Main", "Осн. функционал"}, priority = 1)
    public void A6() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "подписание платежа",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A6_singPaymentFromPagePayments(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A9() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильтр на подписание",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A9_FilterOnSigning(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A10() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильтр все",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A10_FilterAll(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A11() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильтр ни одного",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A11_FilterNoOne(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A12() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильт к отправке в банк",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A12_FilterToBeSentToTheBank(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A13() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильтр на исполнении",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A13_FilterOnThePerformance(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A14() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "фильтр возвращенные",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A14_FilterReturned(driver);
    }

   @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A15() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "Фильтр отправленные в банк",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A15_FilterSentToTheBank(driver);
    }

    @Test(enabled = true, groups = {"Main", "Фильтры"}, priority = 3)
    public void A16() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "выбрать все платежи",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A16_ChooseAllPayments(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A17() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "удалить несколько платежей",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A17_DeleteSeveralPaymentsOnSigning(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A18() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "подписать несколько платежей",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A18_MassSigning(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A19() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "отправить несколько платежей",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A19_MassDeletePaymentsToBeSentToTheBank(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A20() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "отправить несколько платежей",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A20_MassSentPaymentsToBeSentToTheBank(driver);
    }

     @Test(enabled = true, groups = {"Main"}, priority = 4)
    public void A21() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "отправить платеж",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A21_SentPayments(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A22() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "сохранить и подписать",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A22_SaveAndSing(driver);
    }

    @Test(enabled = true, groups = {"Main", "Массовые операции"}, priority = 4)
    public void A23() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "кнопка очистить",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A23_CleanButton(driver);
    }

   /* @Test(enabled = true, groups = {"Main"}, priority = 1)
    public void A24() throws Exception {
        CustomMethods.addTestNameToTheReport(
                "авторизация",
                Thread.currentThread().getStackTrace()[1].toString()
        );
        testSuite.A24_Test(driver);
    }*/
}


