package autoTests;

import autoTests.pages.main.*;
import com.sun.webkit.event.WCMouseWheelEvent;
import com.sun.webkit.perf.PerfLogger;
import com.sun.xml.internal.messaging.saaj.util.transform.EfficientStreamingTransformer;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class TestSuite extends CustomMethods {
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    //<editor-fold desc="A1 - Авторизация п24 ТК11.">
    public void A1_login(WebDriver driver) throws Exception {
        addStepToTheReport("1. Перейти на страницу авторизации");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.getPage();

        addStepToTheReport("2. Авторизоваться");
        Thread.sleep(configVariables.pause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(loginPage.login));
        loginPage.login.sendKeys(configVariables.loginp24);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(loginPage.pass));
        loginPage.pass.sendKeys(configVariables.passp24);

        loginPage.buttonLogin.click();
        Thread.sleep(configVariables.pause);

    }

    //</editor-fold desc="A2 - Настройки">
    public void A3_Setting(WebDriver driver) throws Exception {
        Thread.sleep(configVariables.pause);
        addStepToTheReport("1. Перейти на страницу настроек");
        SettingPage settingPage = new SettingPage(driver);
        settingPage.getPage();

        addStepToTheReport("2. Установить подпись через ЕЦП");
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(settingPage.electronicSignature));
        settingPage.electronicSignature.click();

        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.getPage();
    }

    //</editor-fold desc="A3 -  Страница Платежи, сменить фирму">
    public void A2_Payments(WebDriver driver) throws Exception {
        addStepToTheReport("1. Перейти на страницу платежей");
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.getPage();

        Thread.sleep(configVariables.longPause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.paymentsLink));
        paymentsPage.paymentsLink.click();

        addStepToTheReport("2. Сменить фирму");
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.firmSelection));
        paymentsPage.firmSelection.click();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.firmTestMSB));
        paymentsPage.firmProgrammersMSBTest.click();
    }

    //</editor-fold desc="A4 -  Создать платеж">
    public void A4_CreatePayment(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        FileWorker fileWorker = new FileWorker();
        Parser parser = new Parser();
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        int numberOfPayments = 3;

        PaymentModel paymentModelObject = new PaymentModel();

        addStepToTheReport("1. Перейти на страницу платеж");
        paymentsPage.getPage();

        addStepToTheReport("2. Нажать кнопку создать платеж");
        Thread.sleep(configVariables.pause);
        for (int i = 1; i <= numberOfPayments; i++) {

            paymentsPage.getPage();
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(createPaymentsPage.buttonCreatePayments));
            createPaymentsPage.buttonCreatePayments.click();

            addStepToTheReport("3. Выбрать файл из которого будут подтягиваться реквизиты платежа");
            String fileName = new String(paymentModelObject.SelectFile(i));


            addStepToTheReport("4. Получить реквизиты платежа");
            createPaymentsPage.CompletePaymentOptions(driver, parser.CreatePaymentModel(fileWorker.ReadFile(fileName)));

            addStepToTheReport("6. Сохранить платеж");
            createPaymentsPage.paymentSave.click();
            Thread.sleep(configVariables.longPause);

            switch (i) {
                case 1: {
                    new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
                    paymentsPage.newObject.click();
                    Thread.sleep(configVariables.pause);
                    objectPaymentPage.CheckPaymentOptions(driver, parser.CreatePaymentModel(fileWorker.ReadFile(fileName)));
                    System.out.println("Платеж на счет ПБ успешно создан");
                    break;
                }

                case 2: {
                    new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
                    paymentsPage.newObject.click();
                    Thread.sleep(configVariables.pause);
                    objectPaymentPage.CheckPaymentOptions(driver, parser.CreatePaymentModel(fileWorker.ReadFile(fileName)));
                    System.out.println("Платеж на счет другого банка успешно создан");
                    break;
                }

                case 3: {
                    Thread.sleep(configVariables.pause);
                    Assert.assertTrue(createPaymentsPage.errorWindow.isDisplayed());
                    System.out.println("Указан неверный МФО, получено сообщение об ошибке");
                    break;
                }
            }
        }
        System.out.println("Тест пройден");
    }

    //</editor-fold desc="A5 -  Удаление платежа">
    public void A5_removalPayment(WebDriver driver) throws Exception {
        addStepToTheReport("1. Открыть платеж");
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.getPage();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        // сохранить номер платежа
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.paymentNumber));
        String paymentNumber = new String(objectPaymentPage.paymentNumber.getText());
        addStepToTheReport("2. Удалить платеж");
        Thread.sleep(configVariables.superLongPause);
        objectPaymentPage.removalPayment.click();


        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        Thread.sleep(configVariables.pause);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.paymentNumber));
        // сравнить номер платежа удаленного с платежем первым в списке
        Assert.assertFalse(objectPaymentPage.paymentNumber.getText().equalsIgnoreCase(paymentNumber));
        System.out.println("Платеж удален");
    }

    //</editor-fold desc="A6 -  Подписание платежа">
    public void A6_singPaymentFromPagePayments(WebDriver driver) throws Exception {
        addStepToTheReport("1. Перейти на страницу платежей");
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        CustomMethods customMethods = new CustomMethods();
        paymentsPage.getPage();

        int numberOfElements = 20;
        int pageNumber = 0;
        int paymentsPosition = 1;
        Thread.sleep(configVariables.longPause);
        for (int i = 0; i < numberOfElements; i++) {

            String paymentStatus = customMethods.jsExec(driver, "return $('.archive_actions').children()[" + i + "].innerHTML;");
            paymentsPosition = i + 1;
            WebElement payment = driver.findElement(By.xpath("html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + paymentsPosition + "]/td[3]"));
            if (paymentStatus.equalsIgnoreCase("Подписать")) {

                addStepToTheReport("2. Перейти в платеж");
                payment.click();
                //System.out.println(driver.findElement(By.xpath("html/body/div[1]/div[3]/div/form/div[1]/p")).getText());

                addStepToTheReport("3.  Нажать подписать платеж");
                objectPaymentPage.signPayment.click();
                addStepToTheReport("4.  Выбрать ключ");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('.selectKeyAlias').val('C:/Bykova/Kye/Програмiсти та Ko МСБ-ТЕСТ ТОВ/3383208908_31451288.jks');");

                customMethods.jsExecObj(
                        driver,
                        "return $('.ui-dialog-buttonset').children()[1].click();");
                addStepToTheReport("4. Ввести пароль от ключа");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('.password.marginLeft2percent.marginTop05em').val('123Yecz123');");

                customMethods.jsExecObj(
                        driver,
                        "return $('#otp_prove_ok').click();");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('#otp_prove_ok').click();");
                break;
            }
            if (i == numberOfElements - 1) {

                System.out.println("Нет платежей на подпись на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                i = 0;
                Thread.sleep(configVariables.longPause);
            }

        }
        addStepToTheReport("5. Найти нужный платеж");
        paymentsPage.GoToTheDesiredPage(pageNumber);
        WebElement payment = paymentsPage.FindPayment(driver, paymentsPosition);
        payment.click();
        addStepToTheReport("6. Проверить статус платежа");
        Assert.assertFalse(objectPaymentPage.paymentStatus.getText().equalsIgnoreCase("Создан."));
        System.out.println("Платеж подписан");
    }


    //</editor-fold desc="A7 -  Изменить платеж">
    public void A7_ChangePayment(WebDriver driver) throws Exception {
        addStepToTheReport("1. Открыть платеж");
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.getPage();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        // сохраняем счет получателя
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.recipientAccount));
        String recipientAccount = new String(objectPaymentPage.recipientAccount.getText());
        addStepToTheReport("2. Изменить платеж");
        Thread.sleep(configVariables.superLongPause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.changePayment));
        objectPaymentPage.changePayment.click();

        Thread.sleep(configVariables.pause);

        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(createPaymentsPage.recipientAccountChange));
        createPaymentsPage.recipientAccountChange.clear();

        if (recipientAccount.equalsIgnoreCase("26005060193063")) {
            createPaymentsPage.recipientAccountChange.sendKeys("37391828205001");
        } else {
            createPaymentsPage.recipientAccountChange.sendKeys("26005060193063");
        }

        createPaymentsPage.paymentSaveChange.click();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.recipientAccount));
        // сравнить счет получателя до изменения и после
        Assert.assertFalse(objectPaymentPage.recipientAccount.getText().equalsIgnoreCase(recipientAccount));
        System.out.println("Платеж изменен");

    }

    //</editor-fold desc="A8 -  Копировать платеж">
    public void A8_CopyPayment(WebDriver driver) throws Exception {
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        addStepToTheReport("1. Открыть страницу платежей");
        paymentsPage.getPage();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        addStepToTheReport("2. Сохранить параметры исходного платежа");
        // сохраняем параметры исходного платежа
        String recipientAccount = new String(objectPaymentPage.recipientAccount.getText());
        String senderAccount = new String(objectPaymentPage.senderAccount.getText());
        String okpo = new String(objectPaymentPage.okpo.getText());
        String paymentName = new String(objectPaymentPage.paymentName.getText());
        String bankCode = new String(objectPaymentPage.bankCode.getText());
        String paymentSum = new String(objectPaymentPage.paymentSum.getText());
        String paymentCurrency = new String(objectPaymentPage.paymentCurrency.getText());
        String paymentDetails = new String(objectPaymentPage.paymentDetails.getText());
        addStepToTheReport("3. Скопировать платеж");
        // копируем платеж
        Thread.sleep(configVariables.longPause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.copyPayment));
        objectPaymentPage.copyPayment.click();
        Thread.sleep(configVariables.longPause);
        createPaymentsPage.paymentName.clear();
        Thread.sleep(configVariables.longPause);
        createPaymentsPage.paymentName.sendKeys(paymentName + " copy");
        createPaymentsPage.paymentSave.click();
        Thread.sleep(configVariables.superLongPause);
        addStepToTheReport("4. Скопировать платеж");
        // отркываем скопированый платеж
        paymentsPage.getPage();
        Thread.sleep(configVariables.pause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();
        addStepToTheReport("5. Сравнить платежи");
        //сравниваем данные исходного платежа и созданного
        Assert.assertEquals(objectPaymentPage.recipientAccount.getText(), recipientAccount);
        Assert.assertEquals(objectPaymentPage.senderAccount.getText(), senderAccount);
        Assert.assertEquals(objectPaymentPage.okpo.getText(), okpo);
        Assert.assertEquals(objectPaymentPage.paymentName.getText(), paymentName + " copy");
        Assert.assertEquals(objectPaymentPage.bankCode.getText(), bankCode);
        Assert.assertEquals(objectPaymentPage.paymentSum.getText(), paymentSum);
        Assert.assertEquals(objectPaymentPage.paymentCurrency.getText(), paymentCurrency);
        Assert.assertEquals(objectPaymentPage.paymentDetails.getText(), paymentDetails);
        System.out.println("Платеж скопирован");
    }

    //</editor-fold desc="A9 -  Фильтр на подпись">
    public void A9_FilterOnSigning(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);

        int numberOfElements = 20;
        int numberOfSelectedElements = 0;
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);
        outerloop:
        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр на подписание");
            paymentsPage.onSigning.click();

            Thread.sleep(configVariables.pause);

            for (int i = 1; i <= numberOfElements; i++) {
                if (paymentsPage.FindCheckbox(driver, i).getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {

                    if (paymentsPage.FindStatusButton(driver, i).getAttribute("data-status") != "prepared") {
                        System.out.println(driver.findElement(By.xpath(
                                "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + i + "]/td[2]")).getAttribute("title")
                                + "  Status the right");
                        numberOfSelectedElements++;
                    } else {
                        System.out.println(" Выбран платеж с неверным статусом");
                        return;
                    }
                }
            }
            Thread.sleep(configVariables.pause);

            if (numberOfSelectedElements == 0) {
                System.out.println("Нет платежей удовлетворяющих условие фильтра на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
                break outerloop;
            } else {
                return;
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A10 -  Фильтр все">
    public void A10_FilterAll(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();

        Thread.sleep(configVariables.pause);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
        paymentsPage.filterPayments.click();

        Thread.sleep(configVariables.pause);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
        addStepToTheReport("2. Выбрать фильтр все");
        paymentsPage.allPayments.click();

        int numberOfElements = 20;
        for (int i = 1; i <= numberOfElements; i++) {
            Assert.assertEquals(paymentsPage.FindCheckbox(driver, i).getAttribute("class"), "group_action checkbox active");
        }
        System.out.println("Выбраны все элементы");


    }

    //</editor-fold desc="A11 -  Фильтр ни одного ">
    public void A11_FilterNoOne(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
        paymentsPage.filterPayments.click();

        Thread.sleep(configVariables.pause);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
        addStepToTheReport("2. Выбрать фильтр ни одного");
        paymentsPage.checkNone.click();

        int numberOfElements = 20;

        for (int i = 1; i <= numberOfElements; i++) {
            Assert.assertEquals(paymentsPage.FindCheckbox(driver, i).getAttribute("class"), "group_action checkbox");
        }
        System.out.println("Не выбрано ни одного элемента");

    }

    //</editor-fold desc="A12 -  Фильтр к отправке в банк">
    public void A12_FilterToBeSentToTheBank(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        int numberOfElements = 20;
        int numberOfSelectedElements = 0;
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);


        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр к отправке в банк");
            paymentsPage.toBeSentToTheBank.click();
            Thread.sleep(configVariables.pause);

            for (int i = 1; i <= numberOfElements; i++) {
                if (paymentsPage.FindCheckbox(driver, i).getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {

                    if (paymentsPage.FindStatusButton(driver, i).getAttribute("data-status") != "signed") {
                        System.out.println(driver.findElement(By.xpath(
                                "html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + i + "]/td[2]")).getAttribute("title")
                                + "  Status the right");
                        numberOfSelectedElements++;
                    } else {
                        System.out.println("Выбран платеж с неверным статусом");
                        return;
                    }
                }
            }
            Thread.sleep(configVariables.pause);

            if (numberOfSelectedElements == 0) {
                System.out.println("Нет платежей удовлетворяющих условие фильтра на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            } else {
                System.out.println("тест пройден");
                return;
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A13 -  Фильтр на исполнении">
    public void A13_FilterOnThePerformance(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);

        int numberOfElements = 20;
        int numberOfSelectedElements = 0;
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.longPause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            Thread.sleep(configVariables.longPause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onThePerformance));
            addStepToTheReport("2. Выбрать фильтр на исполнении");
            paymentsPage.onThePerformance.click();

            Thread.sleep(configVariables.longPause);


            for (int i = 1; i <= numberOfElements; i++) {

                if (paymentsPage.FindCheckbox(driver, i).getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {
                    numberOfSelectedElements++;
                }
            }

            Thread.sleep(configVariables.pause);

            if (numberOfSelectedElements == 0) {
                System.out.println("Нет платежей удовлетворяющих условие фильтра на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            } else {
                System.out.println("тест пройден");
                return;
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A14 -  Фильтр возвращеные">
    public void A14_FilterReturned(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);

        int numberOfElements = 20;
        int numberOfSelectedElements = 0;
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);


        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            Thread.sleep(configVariables.longPause);

            addStepToTheReport("2. Выбрать фильтр возвращеные");
            paymentsPage.returned.click();

            Thread.sleep(configVariables.superLongPause);

            for (int i = 1; i <= numberOfElements; i++) {

                if (paymentsPage.FindCheckbox(driver, i).getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {
                        numberOfSelectedElements++;
                }
            }
            Thread.sleep(configVariables.longPause);

            if (numberOfSelectedElements == 0) {
                System.out.println("Нет платежей удовлетворяющих условие фильтра на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            } else {
                System.out.println("тест пройден");
                return;
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A15 -  Фильтр отправленные в банк">
    public void A15_FilterSentToTheBank(WebDriver driver) throws Exception {

        PaymentsPage paymentsPage = new PaymentsPage(driver);
        int numberOfElements = 20;
        int numberOfSelectedElements = 0;
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onThePerformance));
            addStepToTheReport("2. Выбрать фильтр oтправленные в банк");
            paymentsPage.sentToTheBank.click();

            Thread.sleep(configVariables.pause);

            for (int i = 1; i <= numberOfElements; i++) {

                if (paymentsPage.FindCheckbox(driver, i).getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {
                    numberOfSelectedElements++;
                }
            }
            Thread.sleep(configVariables.pause);

            if (numberOfSelectedElements == 0) {
                System.out.println("Нет платежей удовлетворяющих условие фильтра на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            } else {
                System.out.println("Платежи выбраны");
                return;
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A16 -  выбрать все">

    public void A16_ChooseAllPayments(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();

        addStepToTheReport("2. Выбрать все платежи");
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
        paymentsPage.chooseAll.click();

        int count = 0;
        for (int i = 1; i <= 20; i++) {
            WebElement checkbox = driver.findElement(By.xpath("html/body/div[1]/div[3]/div/div[3]/table/tbody/tr[" + i + "]/td[1]/span"));
            if (checkbox.getAttribute("class").equalsIgnoreCase("group_action checkbox active")) {
                count++;
                System.out.println(count + " check");
            }
        }
        Assert.assertEquals(count, 20);
    }

    //</editor-fold desc="A17 -  удалить несколько платежей на подпись">
    public void A17_DeleteSeveralPaymentsOnSigning(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        CustomMethods customMethods = new CustomMethods();
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр на подписание");
            paymentsPage.onSigning.click();

            addStepToTheReport("3. Удалить платежи");
            if (paymentsPage.deleteButton.isDisplayed()) {
                paymentsPage.deleteButton.click();

                Thread.sleep(configVariables.pause);
                customMethods.jsExecObj(
                        driver,
                        "return $('.ui-button-text').click();");
                System.out.println("Платежи удалены");
                return;
            } else {
                System.out.println("Нет платежей со статусом на подпись, для удаления на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A18 -  подписать несколько платежей">
    public void A18_MassSigning(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        CustomMethods customMethods = new CustomMethods();
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр на подписание");
            paymentsPage.onSigning.click();

            addStepToTheReport("3. Нажать подписать");
            if (paymentsPage.massSigning.isDisplayed()) {
                paymentsPage.massSigning.click();

                customMethods.jsExecObj(
                        driver,
                        "return $('.keyPath.firstDisplayNone input').val('C:/Bykova/Kye/Програмiсти та Ko МСБ-ТЕСТ ТОВ/3383208908_31451288.jks');");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('.ui-dialog-buttonset').children()[1].click();");
                addStepToTheReport("4. Ввести пароль от ключа");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('.password.marginLeft2percent.marginTop05em').val('123Yecz123');");

                customMethods.jsExecObj(
                        driver,
                        "return $('#otp_prove_ok').click();");
                Thread.sleep(configVariables.pause);

                customMethods.jsExecObj(
                        driver,
                        "return $('#otp_prove_ok').click();");

                System.out.println("Платежи подписаны");
                Thread.sleep(configVariables.superLongPause);
                return;

            } else {
                System.out.println("Нет платежей на подпись а странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }


    //</editor-fold desc="A19 -  удалить несколько платежей к отправке в банк">
    public void A19_MassDeletePaymentsToBeSentToTheBank(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        CustomMethods customMethods = new CustomMethods();
        int pageNumber = 0;
        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр к отправке в банк");
            paymentsPage.toBeSentToTheBank.click();
            Thread.sleep(configVariables.pause);

            if (paymentsPage.deleteButton.isDisplayed()) {
                paymentsPage.deleteButton.click();

                Thread.sleep(configVariables.superLongPause);

                new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.confirmDeletion));

                customMethods.jsExecObj(
                        driver,
                        "return $('.ui-button-text').click();");

                System.out.println("Платежи удалены");
                return;

            } else {
                System.out.println("Нет платежей со статусом к отправке в банк доступных для удаления на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            }
        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");

    }

    //</editor-fold desc="A20 -  отправить несколько платежей к отправке в банк">
    public void A20_MassSentPaymentsToBeSentToTheBank(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        int pageNumber = 0;

        addStepToTheReport("1. Перейти на страницу платежей");
        paymentsPage.getPage();
        Thread.sleep(configVariables.superLongPause);

        while (paymentsPage.nextPage.isDisplayed()) {
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.filterPayments));
            paymentsPage.filterPayments.click();
            Thread.sleep(configVariables.pause);

            new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.onSigning));
            addStepToTheReport("2. Выбрать фильтр к отправке в банк");
            paymentsPage.toBeSentToTheBank.click();
            Thread.sleep(configVariables.pause);

            if (paymentsPage.sendButton.isDisplayed()) {
                paymentsPage.sendButton.click();
                System.out.println("Платежи отправлены");
                return;
            } else {
                System.out.println("Нет платежей со статусом к отправке в банк доступных для отправки на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                Thread.sleep(configVariables.longPause);
            }

        }
        System.out.println("Нет платежей удовлетворяющих условие поиска");
    }

    //</editor-fold desc="A21 -  отправить платеж">
    public void A21_SentPayments(WebDriver driver) throws Exception {
        addStepToTheReport("1. Перейти на страницу платежей");
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        CustomMethods customMethods = new CustomMethods();

        paymentsPage.getPage();

        int numberOfElements = 20;
        int pageNumber = 0;
        int paymentsPosition = 1;
        Thread.sleep(configVariables.longPause);

        for (int i = 0; i < numberOfElements; i++) {

            String paymentStatus = customMethods.jsExec(driver, "return $('.archive_actions').children()[" + i + "].innerHTML;");
            paymentsPosition = i + 1;
            if (paymentStatus.equalsIgnoreCase("Отправить")) {
                addStepToTheReport("4. Отправить платеж");
                paymentsPage.FindPayment(driver, paymentsPosition).click();
                System.out.println(driver.findElement(By.xpath("html/body/div[1]/div[3]/div/form/div[1]/p")).getText());
                new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.sendPaymentButton));
                objectPaymentPage.senderAccount.click();
                System.out.println("Платеж отправлен");
                break;
            }
            if (i == numberOfElements - 1) {

                System.out.println("Нет платежей на отпрувку на странице " + pageNumber);
                paymentsPage.NextPageButton();
                pageNumber++;
                i = 0;
                Thread.sleep(configVariables.longPause);
            } else {
                System.out.println("Платеж отправлен");
                return;
            }
        }
        addStepToTheReport("5. Найти нужный платеж");
        paymentsPage.GoToTheDesiredPage(pageNumber);
        WebElement payment = paymentsPage.FindPayment(driver, paymentsPosition);
        payment.click();
        addStepToTheReport("6. Проверить статус платежа");
        System.out.println(driver.findElement(By.xpath("html/body/div[1]/div[3]/div/form/div[1]/p")).getText());
        Assert.assertEquals(objectPaymentPage.paymentStatus.getText(), "В обработке.");
        System.out.println("Платеж отправлен");
    }

    //</editor-fold desc="A22 -  сохранить и подписать платеж платеж">
    public void A22_SaveAndSing(WebDriver driver) throws Exception {
        addStepToTheReport("1. Перейти на страницу платеж");
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        CustomMethods customMethods = new CustomMethods();
        PaymentModel paymentModelObject = new PaymentModel();
        Parser parser = new Parser();

        Thread.sleep(configVariables.pause);
        paymentsPage.getPage();

        Thread.sleep(configVariables.pause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(createPaymentsPage.buttonCreatePayments));
        createPaymentsPage.buttonCreatePayments.click();

        addStepToTheReport("2. Создать платеж");

        String fileName = new String(paymentModelObject.SelectFile(1));
        FileWorker fileWorker = new FileWorker();
        createPaymentsPage.CompletePaymentOptions(driver, parser.CreatePaymentModel(fileWorker.ReadFile(fileName)));

        addStepToTheReport("3. Нажать кнопуку сохранить и подписать");
        createPaymentsPage.saveAndSingButton.click();
        Thread.sleep(configVariables.pause);

        customMethods.jsExecObj(
                driver,
                "return $('.keyPath.firstDisplayNone input').val('C:/Bykova/Kye/Програмiсти та Ko МСБ-ТЕСТ ТОВ/3383208908_31451288.jks');");

        customMethods.jsExecObj(
                driver,
                "return $('.ui-dialog-buttonset button:last').click();");
        addStepToTheReport("4. Ввести пароль от ключа");
        Thread.sleep(configVariables.pause);

        customMethods.jsExecObj(
                driver,
                "return $('.password.marginLeft2percent.marginTop05em').val('123Yecz123');");

        customMethods.jsExecObj(
                driver,
                "return $('#otp_prove_ok').click();");
        Thread.sleep(configVariables.pause);

        customMethods.jsExecObj(
                driver,
                "return $('#otp_prove_ok').click();");
        Thread.sleep(configVariables.pause);

        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();
        Thread.sleep(configVariables.pause);

        addStepToTheReport("5. Проверить статус платежа");
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(objectPaymentPage.paymentStatus));
        Assert.assertFalse(objectPaymentPage.paymentStatus.getText().equalsIgnoreCase("Создан."));
        System.out.println("Платеж сохранен и подписан");

    }

    //</editor-fold desc="A23 -  очистить ">
    public void A23_CleanButton(WebDriver driver) throws Exception {

        PaymentsPage paymentsPage = new PaymentsPage(driver);
        ObjectPaymentPage objectPaymentPage = new ObjectPaymentPage(driver);
        CreatePaymentsPage createPaymentsPage = new CreatePaymentsPage(driver);
        PaymentModel paymentModelObject = new PaymentModel();
        FileWorker fileWorker = new FileWorker();
        Parser parser = new Parser();

        addStepToTheReport("1. Перейти на страницу платеж");
        paymentsPage.getPage();
        Thread.sleep(configVariables.pause);

        addStepToTheReport("2. Нажать создать платеж");
        createPaymentsPage.buttonCreatePayments.click();

        Thread.sleep(configVariables.longPause);

        Thread.sleep(configVariables.pause);
        createPaymentsPage.CheckDoesTheEmptyField(driver, 1);

        addStepToTheReport("4. Зайти в платеж");
        paymentsPage.getPage();

        Thread.sleep(configVariables.longPause);
        new WebDriverWait(driver, configVariables.waitPageForLoad).until(ExpectedConditions.visibilityOf(paymentsPage.newObject));
        paymentsPage.newObject.click();

        Thread.sleep(configVariables.superLongPause);

        addStepToTheReport("5. Нажать копировать платеж");
        objectPaymentPage.copyPayment.click();
        Thread.sleep(configVariables.pause);
        createPaymentsPage.CheckDoesTheEmptyField(driver, 2);

    }


   /* public void A24_Test(WebDriver driver) throws Exception {
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.GoToTheDesiredPage(2);
        paymentsPage.FindPayment(driver, 19).click();
        //Thread.sleep(100000);

    }*/

}



