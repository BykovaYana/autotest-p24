package autoTests;

import java.io.*;

/**
 * Created by R2D2 on 15.03.2016.
 */
public class FileWorker {

    public static String ReadFile(String pathFile) throws FileNotFoundException {
        //Этот спец. объект для построения строки
        StringBuilder stringBuilder = new StringBuilder();
        File file = new File(pathFile);

        try {
            //есть ли файл
            file.exists();
            //Объект для чтения файла в буфер
            FileReader fileReader = new FileReader (file);
            BufferedReader in = new BufferedReader(fileReader);
            try {
                //В цикле построчно считываем файл
                String s;
                while ((s = in.readLine()) != null) {
                    stringBuilder.append(s);
                    stringBuilder.append("\n");
                }
            } finally {
                //Также не забываем закрыть файл
                in.close();
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }


        //Возвращаем полученный текст с файла
        return stringBuilder.toString();
    }

    private static void exists(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()){
            throw new FileNotFoundException(file.getName());
        }
    }



}
