package autoTests;

/**
 * Created by R2D2 on 15.03.2016.
 */
public class Parser {

    public PaymentModel CreatePaymentModel(String PaymentFileContent) {
        String[] splitedArray = PaymentFileContent.split("\n");
        PaymentModel paymentModelObject = new PaymentModel(splitedArray[0],splitedArray[1],
                splitedArray[2], splitedArray[3], splitedArray[4], splitedArray[5]);

        /*paymentModelObject.setBankCode(splitedArray[0]);
        paymentModelObject.setOkpo(splitedArray[1]);
        paymentModelObject.setPaymentDetails(splitedArray[2]);
        paymentModelObject.setPaymentName(splitedArray[3]);
        paymentModelObject.setPaymentSum(splitedArray[4]);
        paymentModelObject.setRecipientAccount(splitedArray[5]);*/
        return paymentModelObject;
    }

}
