package autoTests;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class ConfigurationVariables
{
	//используем Singleton
	private static final ConfigurationVariables instance;

	private static String configFilePath = "src/test/resources/config.properties";
	private static Properties configurationData = new Properties();

	public static void fillMyProperties(Properties properties, String filePath)
	{
		InputStreamReader input;
		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream(filePath);
			input = new InputStreamReader(fileInputStream, "UTF8");

			// считываем свойства
			properties.load(input);
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println("Ошибка. Файл config.properties не был найден.");
		}
		catch (java.io.IOException e) {
			System.out.println("IO ошибка в пользовательском методе.");
		}
	}

	private static String getProperty(Properties properties, String propertyKey)
	{
		// получаем значение свойства
		return properties.getProperty(propertyKey).toString();
	}

	public String currentBrowser;
	public int pause;
	public int waitPageForLoad;
	public int superLongPause;
	public int technicalPause;
	public int implicitTimeWait;
	public int longPause;
	public String chromeDriverDirectory;
	public String firefoxDirectoryLocalMachine;
	public String chromeDirectoryLocalMachine;
	public String baseUrl;
	public String loginp24;
	public String passp24;
	public String settingurl;
	public String playmetsurl;
	public String keypass;
	public String cryptoPlugin;
	public String paymentsModelFiles;

	static
	{
		fillMyProperties(configurationData, configFilePath);
		instance = new ConfigurationVariables();
	}

	private ConfigurationVariables()
	{
		/********************************************** конфигурационные данные ***************************************/
		currentBrowser = getProperty(configurationData, "currentBrowser");
		firefoxDirectoryLocalMachine = getProperty(configurationData, "firefoxDirectoryLocalMachine");
		chromeDirectoryLocalMachine = getProperty(configurationData, "chromeDirectoryLocalMachine");
		chromeDriverDirectory = getProperty(configurationData, "chromeDriverDirectory");

		pause = Integer.parseInt(getProperty(configurationData, "pause"));
		waitPageForLoad = Integer.parseInt(getProperty(configurationData, "waitPageForLoad"));
		longPause = Integer.parseInt(getProperty(configurationData, "longPause"));
		superLongPause = Integer.parseInt(getProperty(configurationData, "superLongPause"));
		technicalPause = Integer.parseInt(getProperty(configurationData, "technicalPause"));
		implicitTimeWait = Integer.parseInt(getProperty(configurationData, "implicitTimeWait"));

		baseUrl = getProperty(configurationData, "baseurl");
		settingurl = getProperty(configurationData, "settingurl");
		passp24 = getProperty(configurationData, "passp24");
		loginp24 = getProperty(configurationData, "loginp24");
		playmetsurl = getProperty(configurationData, "playmetsurl");
		keypass = getProperty(configurationData, "keypass");
		cryptoPlugin = getProperty(configurationData, "cryptoPlugin");
		paymentsModelFiles = getProperty(configurationData, "paymentsModelFiles");
	}

	//возвращаем инстанс объекта
	public static ConfigurationVariables getInstance()
	{
		return instance;
	}
}