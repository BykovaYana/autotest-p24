package autoTests;

import java.util.Collection;

/**
 * Created by R2D2 on 14.03.2016.
 */
public class PaymentModel {
    ConfigurationVariables configVariables = ConfigurationVariables.getInstance();

    private String recipientAccount;
    private String okpo;
    private String paymentName;
    private String bankCode;
    private String paymentDetails;
    private String paymentSum;

    public ConfigurationVariables getConfigVariables() {
        return configVariables;
    }

    public void setConfigVariables(ConfigurationVariables configVariables) {
        this.configVariables = configVariables;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(String recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    public String getOkpo() {
        return okpo;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(String paymentSum) {
        this.paymentSum = paymentSum;
    }

    public PaymentModel() {
    }

    public PaymentModel(String recipientAccount,String okpo, String paymentName, String bankCode, String paymentDetails, String paymentSum) {
        this.paymentSum = paymentSum;
        this.paymentSum = paymentSum;
        this.bankCode = bankCode;
        this.paymentName = paymentName;
        this.okpo = okpo;
        this.recipientAccount = recipientAccount;
        this.paymentDetails = paymentDetails;
    }


    public String SelectFile (int  fileNumber){
        String fileName = new String();

        fileName = configVariables.paymentsModelFiles+fileNumber+".txt";

        return fileName;
    }

}
